const {MongoClient,ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err,db) => {
    if (err) {
        return console.log('unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server successfully');
    db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID("5be8544b0877502594f2b482")
    },{
        $set: {
            completed: true
        }
    },{
        upsert: true,
        returnOriginal: false
    }).then((res) => {
        console.log(res)
    });
    // db.close();
});